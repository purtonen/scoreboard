import React from 'react';
import { NavLink } from "react-router-dom";

export function NavLinkExt(props) {
    function unFocus(e){
        e.currentTarget.blur()
    }
    return(
        <NavLink {...props} onClick={unFocus}></NavLink>
    )
}

export default NavLinkExt;