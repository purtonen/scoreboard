import React, { Component } from 'react';
import { ReactComponent as Logo } from './assets/logo5.svg';
import './App.css';
import {
    Route,
    HashRouter
} from "react-router-dom";
import { NavLinkExt as NavLink } from './NavLinkExt.js';
import About from "./About";
import Portfolio from "./Portfolio";

class App extends Component {
    render() {
        return (
            <HashRouter>
                <div className="App">
                    <header className="App-header">
                        <NavLink className="logo-wrapper" to="/">
                            <Logo className="logo" />
                        </NavLink>
                        <nav>
                            <div className="nav-content">
                                <ul>
                                    <li><NavLink to="/about">About</NavLink></li>
                                    <li><NavLink to="/portfolio">Portfolio</NavLink></li>
                                    <li><NavLink to="/contact">Contact</NavLink></li>
                                </ul>
                            </div>
                            <div className="transparent-edge"></div>
                        </nav>
                    </header>

                    <div className="content">
                        <Route path="/about" component={About} />
                    </div>
                    <div className="content">
                        <Route path="/portfolio" component={Portfolio} />
                    </div>
                </div>
            </HashRouter>
        );
    }
}

export default App;
