import React, { Component } from "react";
import Spinner from "./Spinner";
import Arrow from "./Arrow";
import axios from "axios";

class Scoreboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            scores: [],
            order: ''
        }
        this.handleSort = this.handleSort.bind(this);
    }

    // Direction dictionary for 'ASC' = 'up' etc.
    dirDict(order) {
        return {
            '':'',
            'ASC': 'up',
            'DESC': 'down'
        }[order];
    }

    handleSort() {
        // Set order to ascending if it's descending or unset
        const newOrder = this.state.order === 'ASC' ? 'DESC' : 'ASC';

        // Sortable = frontend sort, otherwise fetch sorted from backend
        if (this.props.sortable) {
            const newScores = this.state.scores;
            newScores.sort((a, b) => {
                let bigger = a.score > b.score;
                return newOrder === 'ASC' ? bigger : !bigger;
            });
            console.log(newScores)
            this.setState({ scores: newScores, order: newOrder });
        } else {
            this.setState({loading: true});
            axios.get('http://' + window.location.hostname + ':8001/api/score?sort='+newOrder)
                .then(res => {
                    this.setState({ loading: false, scores: res.data.data, order: newOrder });
                });
        }
    }

    handleNewScore(){
        // TODO
    }

    componentDidMount() {
        axios.get('http://' + window.location.hostname + ':8001/api/score')
            .then(res => {
                this.setState({ loading: false, scores: res.data.data });
            });
    }

    render() {
        if (this.state.loading) {
            return <Spinner />
        }

        return (
            <div className="scoreboard">
                <table>
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th className="clickable" onClick={this.handleSort}>
                            Score <Arrow direction={this.dirDict(this.state.order)}/>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.scores.map(s => {
                            return (
                                <tr key={s.id}>
                                    <td>{s.username}</td>
                                    <td>{s.score}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Scoreboard;