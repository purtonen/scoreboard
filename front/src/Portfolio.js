import React, { Component } from "react";
import Scoreboard from "./Scoreboard";

class About extends Component {
    render() {
        return (
            <div>
                <h2>Scoreboard app</h2>
                <div className="flex flex__center">
                    <div>
                        <h3>Backend sort</h3>
                        <Scoreboard />
                    </div>
                    <div>
                        <h3>Frontend sort</h3>
                        <Scoreboard sortable />
                    </div>
                </div>
            </div>
        );
    }
}

export default About;