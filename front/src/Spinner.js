import React from "react";
import spinner from "./assets/spinner.gif";

function Spinner(){
    return(
        <div className="spinner-wrapper">
            <img className="spinner" src={spinner}></img>
        </div>
    );
}

export default Spinner;