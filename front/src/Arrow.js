import React from "react";

function Arrow(props) {
    let content = "";
    console.log(props)
    switch (props.direction) {
        case 'up':
            content = "\u25B4";
            break;
        case 'down':
            content = "\u25BE";
            break;
        case 'left':
            content = "\u25C2";
            break;
        case 'right':
            content = "\u25B8";
            break;

    }
    return <span className="caret">{content}</span>
}

export default Arrow;