package main

import (
	"fmt"
	"net/url"
	"os"

	db "gitlab.com/purtonen/scoreboard/database"
	log "gitlab.com/purtonen/scoreboard/logging"
	"gitlab.com/purtonen/scoreboard/routes"
)

func init() {
	log.Info("main init")
}

func main() {
	log.Info("main main")
	mongoUser := os.Getenv("MONGO_USER")
	mongoPass := url.QueryEscape(os.Getenv("MONGO_PASSWORD"))
	mongoHost := os.Getenv("MONGO_HOST")
	mongoPort := os.Getenv("MONGO_PORT")
	//mongoDatabase := "admin" //os.Getenv("MONGO_DATABASE")
	db.InitDB(fmt.Sprintf("mongodb://%s:%s@%s:%s/admin", mongoUser, mongoPass, mongoHost, mongoPort))
	routes.StartListening()
}
