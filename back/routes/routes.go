package routes

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/gorilla/mux"

	log "gitlab.com/purtonen/scoreboard/logging"
)

var routes []route
var router *mux.Router

type response struct {
	Code  int         `json:"code"`
	Error string      `json:"error"`
	Data  interface{} `json:"data"`
}

type urlVariables map[string]string
type jsonStruct map[string]interface{}

func errorResponse(code int, errorMsg string) response {
	return response{Code: code, Error: errorMsg}
}

func successResponse(data interface{}) response {
	return response{Code: 0, Data: data}
}

type callback func(urlVariables, url.Values, jsonStruct) response

type route struct {
	url         string
	method      string
	scope       string
	callback    callback
	isSensitive bool
}

func (route route) equals(otherRoute route) bool {
	return route.url == otherRoute.url && route.method == otherRoute.method
}

const (
	baseURL = "/api"
)

func init() {
	fmt.Println("routes init")
	router = mux.NewRouter()
}

func addRoute(url string, method string, scope string, cb callback) error {
	// Create new route object
	newRoute := route{
		url:      baseURL + url,
		method:   method,
		scope:    scope,
		callback: cb,
	}

	// Go through existing routes and makes sure they don't collide
	for _, route := range routes {
		if route.equals(newRoute) {
			log.Errorf("Route %s %s already exists\n", newRoute.method, newRoute.url)
			return errors.New("Route already exists")
		}
	}
	routes = append(routes, newRoute)
	return nil
}

// StartListening : Start the actual HTTP listeners
func StartListening() {
	for _, route := range routes {
		rt := route
		router.HandleFunc(route.url, func(w http.ResponseWriter, r *http.Request) {
			callbackHandler(w, r, rt)
		}).Methods(route.method)
	}

	// Add middlewares
	router.Use(accessLogging, setJSONHeader, allowCORS)

	// Trailing slashes need to be removed before handling anything, Middleware won't work
	log.Fatal(http.ListenAndServe(":8000", removeTrailingSlash(router)))
}

func removeTrailingSlash(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.URL.Path = strings.TrimSuffix(r.URL.Path, "/")
		next.ServeHTTP(w, r)
	})
}

func accessLogging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "GET" {
			log.Tracef("%s %s", r.Method, r.URL.Path)
		}
		next.ServeHTTP(w, r)
	})
}

func setJSONHeader(next http.Handler) http.Handler {
	// Set response type to JSON
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func allowCORS(next http.Handler) http.Handler {
	// allow cross domain AJAX requests
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		next.ServeHTTP(w, r)
	})
}

// callbackHandler : handle incoming traffic and route it to the correct functions
func callbackHandler(w http.ResponseWriter, r *http.Request, route route) {
	var cb callback
	vars := mux.Vars(r)
	params := r.URL.Query()
	body, err := ioutil.ReadAll(r.Body)
	log.CheckError(err)

	// Undefined json structure
	var bodyObject jsonStruct

	// IF body is set, try to marshal into JSON
	if len(body) > 0 {
		err = json.Unmarshal(body, &bodyObject)
		if err != nil {
			log.Error(err.Error())
			errRes := response{Code: 1, Error: err.Error()}
			resJSON, err := json.Marshal(errRes)
			log.CheckError(err)
			w.Write(resJSON)
			return
		}
	}

	// Set default error response
	res := response{Code: -1, Error: "route mismatch"}

	// Call the route callback function if it is set
	cb = route.callback
	if cb != nil {
		res = cb(vars, params, bodyObject)
	}

	// Marshal the callback or default error response to JSON
	resJSON, err := json.Marshal(res)
	log.CheckError(err)

	// Finally write the response JSON back to the HTTP ResponseWriter
	w.Write(resJSON)
}
