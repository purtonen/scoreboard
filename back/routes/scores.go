package routes

import (
	"net/url"
	"reflect"
	"sort"

	"go.mongodb.org/mongo-driver/bson/primitive"

	db "gitlab.com/purtonen/scoreboard/database"
	log "gitlab.com/purtonen/scoreboard/logging"
)

type scoreRow struct {
	ID       primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Username string             `json:"username" bson:"username"`
	Score    int                `json:"score" bson:"score"`
}

func init() {
	addRoute("/score", "GET", "scope", getAllScores)
	addRoute("/score", "POST", "scope", addScore)
	addRoute("/score/{id}", "GET", "scope", getScore)
	addRoute("/score/{id}", "PUT", "scope", addScore)
	addRoute("/score/{id}", "DELETE", "scope", addScore)
}

// GetScoreInterface makes it possible to pass internal
// structs to outside packages
func GetScoreInterface() interface{} {
	return &scoreRow{}
}

func getAllScores(vars urlVariables, params url.Values, body jsonStruct) response {
	res := db.GetAll("scores", GetScoreInterface)
	scores := make([]*scoreRow, len(res))
	for i, v := range res {
		log.Info(reflect.TypeOf(v))
		log.Info(v)
		scores[i] = v.(*scoreRow)
	}
	if order, ok := params["sort"]; ok && len(order) > 0 {
		log.Info(order[0])
		log.Info(reflect.TypeOf(order))
		sort.Slice(scores, func(i, j int) bool {
			if order[0] == "DESC" {
				return scores[i].Score > scores[j].Score
			}
			return scores[i].Score < scores[j].Score
		})
	}
	return successResponse(scores)
}

func getScore(vars urlVariables, params url.Values, body jsonStruct) response {
	res, err := db.GetOne("scores", vars["id"], GetScoreInterface)
	if err != nil {
		return errorResponse(1, err.Error())
	}
	return successResponse(res)
}

func addScore(vars urlVariables, params url.Values, body jsonStruct) response {
	username, ok := body["username"]
	if !ok {
		return errorResponse(1, "username is required")
	}
	score, ok := body["score"]
	if !ok {
		return errorResponse(1, "score is required")
	}

	newScore := scoreRow{Username: username.(string), Score: int(score.(float64))}

	res, err := db.InsertOne("scores", newScore)
	if err != nil {
		return errorResponse(1, err.Error())
	}

	newScore.ID = res.(primitive.ObjectID)
	return successResponse(newScore)
}
