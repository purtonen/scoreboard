package db

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"

	log "gitlab.com/purtonen/scoreboard/logging"
)

var client *mongo.Client
var ctx context.Context
var db *mongo.Database

type ModelFactory func() interface{}

// InitDB : Initialize database connection
func InitDB(dataSourceName string) {
	var err error

	log.Info(dataSourceName)
	// Create mongodb client
	client, err = mongo.NewClient(options.Client().ApplyURI(dataSourceName))
	log.CheckError(err)

	// Connect to the database
	ctx, _ = context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	log.CheckError(err)

	// Ping the server to check it was found
	ctx, _ = context.WithTimeout(context.Background(), 5*time.Second)
	err = client.Ping(ctx, readpref.Primary())
	log.CheckError(err)

	// Set database variable
	db = client.Database("scoreboard")
}

func pageToOffset(page int, limit int) int {
	if page == 0 || page == 1 {
		return 0
	}

	return (page - 1) * limit
}

func InsertOne(coll string, model interface{}) (interface{}, error) {
	collection := db.Collection(coll)
	ctx, _ = context.WithTimeout(context.Background(), 5*time.Second)
	res, err := collection.InsertOne(ctx, model)
	log.CheckError(err)
	if err != nil {
		return nil, err
	}

	return res.InsertedID, nil
}

func GetOne(coll string, id string, mf ModelFactory) (interface{}, error) {
	inf := mf()
	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	collection := db.Collection(coll)
	ctx, _ = context.WithTimeout(context.Background(), 5*time.Second)
	err = collection.FindOne(ctx, bson.M{"_id": objId}).Decode(inf)
	log.CheckError(err)

	if err != nil {
		return nil, err
	}

	return inf, nil
}

func GetAll(coll string, mf ModelFactory) []interface{} {
	collection := db.Collection(coll)
	ctx, _ = context.WithTimeout(context.Background(), 5*time.Second)
	cur, err := collection.Find(ctx, bson.M{})
	log.CheckFatal(err)
	defer cur.Close(ctx)

	var models []interface{}
	for cur.Next(ctx) {
		inf := mf()
		err := cur.Decode(inf)
		log.CheckError(err)
		if err == nil {
			models = append(models, inf)
		}
	}

	return models
}
