package log

import (
	"log"
	"os"
	"runtime/debug"
)

var (
	traceLogger   *log.Logger
	infoLogger    *log.Logger
	warningLogger *log.Logger
	errorLogger   *log.Logger
	fatalLogger   *log.Logger
	debugLogger   *log.Logger
	debugMode     bool
)

// InitLoggers : Inits the loggers
func init() {
	debugMode = os.Getenv("DEBUG") == "true"

	accessLogFile, err := os.Create("./logs/access.log")
	if err != nil {
		log.Fatalln("Fail to open log file")
	}

	infoLogFile, err := os.Create("./logs/info.log")
	if err != nil {
		log.Fatalln("Fail to open log file")
	}

	warningLogFile, err := os.Create("./logs/warning.log")
	if err != nil {
		log.Fatalln("Fail to open log file")
	}

	errLogFile, err := os.Create("./logs/error.log")
	if err != nil {
		log.Fatalln("Fail to open log file")
	}

	debugLogFile, err := os.Create("./logs/debug.log")
	if err != nil {
		log.Fatalln("Fail to open log file")
	}

	traceLogger = log.New(accessLogFile,
		"TRACE: ",
		log.Ldate|log.Ltime)

	infoLogger = log.New(infoLogFile,
		"INFO: ",
		log.Ldate|log.Ltime)

	warningLogger = log.New(warningLogFile,
		"WARNING: ",
		log.Ldate|log.Ltime)

	errorLogger = log.New(errLogFile,
		"ERROR: ",
		log.Ldate|log.Ltime)

	fatalLogger = log.New(errLogFile,
		"FATAL ERROR: ",
		log.Ldate|log.Ltime)

	debugLogger = log.New(debugLogFile,
		"DEBUG: ",
		log.Ldate|log.Ltime)

	infoLogger.Println("Logger init")
	infoLogger.Printf("Debug = %t", debugMode)
}

// SetDebug : sets the debug flag to allow logging to debug.log
func SetDebug(b bool) {
	debugMode = b
}

// Tracef : calls Output to print to the Trace logger. Arguments are handled in the manner of fmt.Printf.
func Tracef(format string, v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	traceLogger.Printf(format, v...)
}

// Trace : calls Output to print to the Trace logger. Arguments are handled in the manner of fmt.Print.
func Trace(v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	traceLogger.Print(v...)
}

// Traceln : calls Output to print to the Trace logger. Arguments are handled in the manner of fmt.Println.
func Traceln(v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	traceLogger.Println(v...)
}

// Infof : calls Output to print to the Info logger. Arguments are handled in the manner of fmt.Printf.
func Infof(format string, v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	infoLogger.Printf(format, v...)
}

// Info : calls Output to print to the Info logger. Arguments are handled in the manner of fmt.Print.
func Info(v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	infoLogger.Print(v...)
}

// Infoln : calls Output to print to the Info logger. Arguments are handled in the manner of fmt.Println.
func Infoln(v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	infoLogger.Println(v...)
}

// Warnignf : calls Output to print to the Warnign logger. Arguments are handled in the manner of fmt.Printf.
func Warnignf(format string, v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	warningLogger.Printf(format, v...)
}

// Warnign : calls Output to print to the Warnign logger. Arguments are handled in the manner of fmt.Print.
func Warnign(v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	warningLogger.Print(v...)
}

// Warnignln : calls Output to print to the Warnign logger. Arguments are handled in the manner of fmt.Println.
func Warnignln(v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	warningLogger.Println(v...)
}

// Errorf : calls Output to print to the Error logger. Arguments are handled in the manner of fmt.Printf.
func Errorf(format string, v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	errorLogger.Printf(format, v...)
}

// Error : calls Output to print to the Error logger. Arguments are handled in the manner of fmt.Print.
func Error(v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	errorLogger.Print(v...)
}

// Errorln : calls Output to print to the Error logger. Arguments are handled in the manner of fmt.Println.
func Errorln(v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	errorLogger.Println(v...)
}

// Fatalf : calls Output to print to the Error logger and then os.Exit(1). Arguments are handled in the manner of fmt.Printf.
func Fatalf(format string, v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	fatalLogger.Fatalf(format, v...)
}

// Fatal : calls Output to print to the Error logger and then os.Exit(1). Arguments are handled in the manner of fmt.Print.
func Fatal(v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	fatalLogger.Fatal(v...)
}

// Fatalln : calls Output to print to the Error logger and then os.Exit(1). Arguments are handled in the manner of fmt.Println.
func Fatalln(v ...interface{}) {
	if debugMode {
		debugLogger.Print(v...)
		debugLogger.Println(string(debug.Stack()))
	}
	fatalLogger.Fatalln(v...)
}

// CheckTrace : calls Output to print to the Trace logger.
func CheckTrace(err error) {
	if err != nil {
		if debugMode {
			debugLogger.Print(err.Error())
			debugLogger.Println(string(debug.Stack()))
		}
		traceLogger.Println(err.Error())
	}
}

// CheckInfo : calls Output to print to the Info logger.
func CheckInfo(err error) {
	if err != nil {
		if debugMode {
			debugLogger.Print(err.Error())
			debugLogger.Println(string(debug.Stack()))
		}
		infoLogger.Println(err.Error())
	}
}

// CheckWarnign : calls Output to print to the Warnign logger.
func CheckWarnign(err error) {
	if err != nil {
		if debugMode {
			debugLogger.Print(err.Error())
			debugLogger.Println(string(debug.Stack()))
		}
		warningLogger.Println(err.Error())
	}
}

// CheckError : calls Output to print to the Error logger.
func CheckError(err error) {
	if err != nil {
		if debugMode {
			debugLogger.Print(err.Error())
			debugLogger.Println(string(debug.Stack()))
		}
		errorLogger.Println(err.Error())
	}
}

// CheckFatal : if an error exists, calls Output to print to the Error logger and then os.Exit(1)
func CheckFatal(err error) {
	if err != nil {
		if debugMode {
			debugLogger.Print(err.Error())
			debugLogger.Println(string(debug.Stack()))
		}
		fatalLogger.Fatalln(err.Error())
	}
}
