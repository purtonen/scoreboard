default: build
	@echo "============= Starting Scoreboard locally ============="
	docker-compose up -d
	
build:
	@echo "============= Building backend ============="
	docker build --rm -t score-back ./back

	# @echo "============= Building frontend ============="
	# docker build --rm -t front ./front

reload:
	@echo "============= Reloading backend ============="
	docker-compose restart score-back

logs:
	docker-compose logs -f

down:
	docker-compose down

test:
	go test -v -cover ./...

clean: down
	@echo "=============cleaning up============="
	rm -f api
	docker system prune -f
	docker volume prune -f
